git checkout

1) `git checkout {branch}` - switches to another branch
2) `git checkout -f` - performs a reset on a branch
3) `git checkout {hash}` - switches branch to detached head state - every commit that is done in this state will be lost when switched to other branch
4) `git checkout -b {branchname}` - creates a new branch from the current HEAD
5) `git checkout master` - switches to master branch
6) `git checkout -- {file}` - changes the file back to state from last commit

Exercise:
1) Create a new branch called feature
2) Switch to feature branch
3) Make some changes to repository
4) Commit the changes
5) Switch to master branch and check state of the files
6) Checkout a commit
7) Make changes to repository
8) Commit the changes
9) Checkout master